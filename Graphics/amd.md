# AMD Graphics
## corectrl
Corectrl is a software to control your AMD GPU settings from a GUI. 
To install simply run:
```
sudo pacman -S corectrl
```
if you would like for it to run on startup, simply follow the official setup wiki for it at:
* https://gitlab.com/corectrl/corectrl/-/wikis/Setup

## Useful Environment Variables
the MESA drivers for AMD do allow for a LOT of customizations. the list of options is too large and changes too often to list here but they are listed all here:
* Vulkan RADV https://docs.mesa3d.org/envvars.html#radv-driver-environment-variables
* OpenGL https://docs.mesa3d.org/envvars.html#radeonsi-driver-environment-variables

to use simply append as a comma-separated list before your software command

```
env RADV_PERFTEST=sam,video_decode,etc RADV_TEX_ANISO=16 [software]

# or for steam games launch options

RADV_PERFTEST=sam,video_decode,etc RADV_TEX_ANISO=16 %command%
```

## Custom MESA gaming driver

if you feel adventurous, you can compile your own amd gaming optimized drivers.

simply run:
```
yay -S mesa-amdonly-gaming-git 
yay -S lib32-mesa-amdonly-gaming-git 
```

if you want even more performances, you can add the following flags to build () in the pkgbuild:

```
       -D optimization=2 \
       -D b_lto=true \
```

to edit the pkgbuild add `--editmenu` to your yay command like so `yay -S --editmenu [pkgname]`

you may need to remove some mesa packages before the system lets you install mesa from the AUR.
* Next ---->[Kernel options](..%2FKernel%2FKernel_options.md)
* [README](..%2FREADME.md)<---- Back to home