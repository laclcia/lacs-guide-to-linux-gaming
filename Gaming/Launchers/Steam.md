# steam
## About
Steam is the main Gaming platform for PC gamers. 
It also happen's to be the main driving force propelling linux gaming.

## Base install
well start by installing the base steam package.
```
sudo pacman -S steam
```
that is all that is needed to run the base steam. 
but here well also go trough how to install custom proton, 
gamescope, steam tinker launch, and how to set steam to use proton for all games.

## Custom proton versions
There are a couple of way's to install custom proton versions but here well use the easiest one.
Protonup
* https://davidotek.github.io/protonup-qt/
```
yay -S protonup-qt-bin
```
with this you can download and install any custom version of not only proton but also some other usefull tool's such as:
### Boxtron
* https://github.com/dreamer/boxtron

compatibility tool to run DOS games using native linux DOSBox
### Luxtorpeda
* https://luxtorpeda.gitlab.io/

Fairly big project that does drop in replacement of many game engine to a modern linux native reimplementation.
### Roberta
* https://github.com/dreamer/roberta

engine reimplementation of old adventure games in the sierra style.
engine replacement is ScummVM
### SteamTinkerLaunch
this tool is great and basically allows for game customizztion like modding. 
well be using a local install of this however and not the protonup version.

## SteamTinkerLaunch
* https://github.com/sonic2kk/steamtinkerlaunch
```
yay -S steamtinkerlaunch 
```
i highly suggest you read the readme on the repo for this tool to understand it's full use.
## Gamescope
this tool runs the game/software you run trough it in it's own virtual monitor.
this can also apply enhancement's like FSR to any games to increase performences.
* https://github.com/ValveSoftware/gamescope
```
sudo pacman -S gamescope
```
this and tinker launch are some of your strongest tools to make full use of your linux gaming system
* Next ---->[Lutris](Lutris.md)
* [README](..%2FREADME.md)<---- Back to home