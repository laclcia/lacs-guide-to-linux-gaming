# Lutris
## About
Lutris is the launcher for "everything else."
If your game isn't from steam, GOG, Epic, and it's a windows game, then Lutris is what you want to use to handle it.

Once installed, lutris offers one-click solutions to install games.
Simply by going to their website, searching the game you want to install, and clicking install.
In most cases, lutris will handle downloading and installation plus it then becomes a launcher for those games and makes customization easy.
* https://lutris.net/
## Installation
```
yay -S lutris-git
```
* Next ---->[Postprocess & Benchmark](..%2Fpostprocess_benchmark.md)
* [README](..%2FREADME.md)<---- Back to home