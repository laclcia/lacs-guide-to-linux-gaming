# Heroic launcher
## About
The heroic launcher is a game launcher for both Epic games and GOG. 
Replacing the epic launcher and GOG galaxy.
It also allows the user to use installed proton versions to run games.
It is the preferred method to run both GOG and Epic games.
* https://heroicgameslauncher.com/
## Installation
```
yay -S heroic-games-launcher-bin
```
* Alternative --->[minigalaxy.md](minigalaxy.md)
* Next ---->[Steam.md](Steam.md)
* [README](..%2FREADME.md)<---- Back to home