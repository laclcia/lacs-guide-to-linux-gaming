# Minigalaxy
## About
Mini galaxy is an alternative to Heroic Games Launcher.
It allows the user to make use of system wine to play GOG games.
It is recommended to use HGL instead.
* --->[Heroic.md](Heroic.md)
## Installation
```
yay -S minigalaxy
```
* Next ---->[Steam.md](Steam.md)
* [README](..%2FREADME.md)<---- Back to home