# Postprocessing shaders, GPU metrics & replay

## GPU Metrics

### Mangohud

#### About

Mangohud provides you with a full set of on-screen metrics for the game you are running.
Similar to how MSI afterburner on windows. 
It is fully customizable by either editing the config file or using Goverlay.
This guide will Go the Goverlay route.
* ---->[Installation](#installation)
* More info ---->https://github.com/flightlessmango/MangoHud
### VKbasalt
#### About
VKbasalt is a postprocessing software similar to Reshade on windows.
It can be customized from either config file or Goverlay.
Unfortunately, VKbasalt does not provide a configurable UI like Reshade.
It does, however, work wonderfully on any Vulkan title.
Including DX titles that are run through DXVK, VKD3D and even OpenGL titles run through Zink.
This guide will Go the Goverlay route.
* ---->[Installation](#installation)
* More Info ---->https://github.com/DadSchoorse/vkBasalt
### ReplaySorcery
If you have ever used AMD ReLive or Nvidia ShadowPlay Instant Replay, then you know what this is.
This constantly records the screen with very low overhead as to not use too many resources.
Once you do something you wish you had recorded, you simply hit a key combo, and it will save the last 30 seconds (or more depending on config).
Really useful for both content creation and for braging right's ^_^
as with the rest well go the Goverlay route for this too.
* ---->[Installation](#installation)
* More Info ---->https://github.com/matanui159/ReplaySorcery
## Installation
To install all of those for both 32bit and 64bit games is simple. 
Goverlay-bin will pull the 64bit version of all of those as a dependency, so we only need to add the lib32 to the installation.

```
yay -S goverlay-bin lib32-mangohud lib32-vkbasalt replay-sorcery reshade-shaders-git
```

* [README](..%2FREADME.md)<---- Back to home