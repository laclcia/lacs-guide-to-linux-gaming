# **Laclica's guide to Linux Gaming**
### **Caution**
This guide is intended and written from the perspective of Arch and Arch based distro's.

what's in here should theoretically work on any distro

everything is written in a concise TL;DR style

## Table of Contents:
### Basic
* ---->[System ricing](Basic%2FSystem_ricing.md)
* ---->[Useful software](Basic%2FUsefull_software.md)
* ---->[Wine](Basic%2FWine.md)
#### AUR
* ---->[Setting up](AUR%2FSetting_up.md)
#### Kernel
* ---->[Kernel options](Kernel%2FKernel_options.md)
* ---->[Custom Kernels](Kernel%2FCustom_Kernels.md)
* ---->[zramswap](Kernel%2Fzramswap.md)
#### Graphics
* ---->[AMD GPU](Graphics%2Famd.md)
#### Gaming
* ---->[HeroicGamesLauncher](Gaming%2FLaunchers%2FHeroic.md)
* ---->[Steam](Gaming%2FLaunchers%2FSteam.md)
* ---->[Minigalaxy](Gaming%2FLaunchers%2Fminigalaxy.md)
* ---->[Lutris](Gaming%2FLaunchers%2FLutris.md)
* ---->[Postprocess & Benchmark](Gaming%2Fpostprocess_benchmark.md)
#### Misc
* ---->[FOSS Games](Misc%2FFOSS_games.md)
* ---->[Thread limit](Misc%2FThread-limit.md)
* ---->[clearer fonts](Misc%2Fclearer_fonts.md)