# Limiting core/thread of a process
sometimes there is a need to limit a software cpu usage for various reasons (preventing freezes, crashes or in certain cases like with monster hunter worlds to increase framerate by choking the game). we can limit the amount of thread any software uses esealy using the `taskset` tool

first let's make sure it's installed
```
sudo pacman -S util-linux
```

normal usage:
taskset -cp [mask] [PID]

let me explain.
the [mask] is the cpu mask in thread's. it is a comma seperated list and could be done like this.
*   0,1,2,3,5

to specify cores 0 trough 3 and core 5. the same can also be done with a - to denote a range like so
*   0-3,5

witch would be the same as the example above. ranges can also have a skip variable to it like so
*   0-7:2

in this case the : denotes a skip step of every other. and would select the following range in that case
*   0,2,4,6

in essence selecting every other core

the second part is the PID or Process ID. witch is the unix process identifier. that is your software's unic identifyer. any top software like bpytop, bashtop, htop and the likes will list the pid next to the command. easy to get just open top and look for the software you whanna limit then coppy or retype the pid listed next to it. just be shure to check the PID every time you relunch the software as PID are dynamic and will change every run.

a completed command look's like this

taskset -cp 0-15:2 2553643

[README](..%2FREADME.md)<---- Back to home