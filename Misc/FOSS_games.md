# Free and Open Source Software Games

## Minetest
Want a highly moddable minecraft like game platform with a cornucopia of games available for free including minecraft like experiences?
Then minetest is for you. C
heck out theyre website if you think it sound interesting.
* https://www.minetest.net/
```
sudo pacman -S minetest
```
## PolyMC
want a native linux multi launcher, instance and mod manager for minecraft?
PolyMC does all that and more. including mod-pack auto installation.
* https://polymc.org/
```
yay -S polymc
```
## Doomrl
This game is a roguelike Doom inspired FPS. and it's free .
* https://drl.chaosforge.org/
```
yay -S doomrl
```
## Thrive
Evolution Sims. 
Spore used to be the only player in town but now Thrive offers a better experience for free and open source.
* https://revolutionarygamesstudio.com/
```
yay -S thrive-bin
```
## Tabletop-hub
Tabletop-hub is a game that allow's you to play tabletop games online with friends with good modding support.
* https://github.com/drwhut/tabletop-club
* https://flathub.org/apps/io.itch.drwhut.TabletopClub
```
flatpak install flathub io.itch.drwhut.TabletopClub
```
## Cavestory
 the OG Cavestory is avalible in a opensource engine remake called NXengine-evo. 
 a classic that has been ported to everything. 
 * https://www.cavestory.org/
 * https://github.com/nxengine/nxengine-evo
```
yay -S nxengine-evo
```

* [README](..%2FREADME.md)<---- Back to home