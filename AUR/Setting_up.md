# the Arch User Repository

The Arch User Repository is one of the greatest advantages of Arch based distro's. It enables you to easily download compile and install non-native packages with minimal fuz/technical knowledge.
Do keep in mind tho that all AUR packages are user submitted and are NOT VETTED. USE AT YOUR OWN RISK.

## Presetup MAKEPKG:

Makepkg is at the core of using the AUR. and before we proceed further, let's optimize it so that all our packages are optimally compiled.

1. we install required dependencies with the following command

```
sudo pacman -S pigz pbzip2 ccache base-devel
```

2. well coppy the system makepkg.conf to our home by running the following
```
cp /etc/makepkg.conf ~/.makepkg.conf
```
3. well edit it now using nano
```
nano ~/.makepkg.conf
```
find the section that start's with:

```
# ARCHITECTURE, COMPILE FLAGS
```

and well modify it to look like the following:

```
#########################################################################
# ARCHITECTURE, COMPILE FLAGS
#########################################################################
#
CARCH="x86_64"
CHOST="x86_64-pc-linux-gnu"

#-- Compiler and Linker Flags
#CPPFLAGS=""
CFLAGS="-march=native -mtune=native -O2 -pipe -fno-plt -fexceptions \
        -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security \
        -fstack-clash-protection -fcf-protection"
CXXFLAGS="$CFLAGS -Wp,-D_GLIBCXX_ASSERTIONS"
LDFLAGS="-Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now"
RUSTFLAGS="-C opt-level=2 -C target-cpu=native"
#-- Make Flags: change this for DistCC/SMP systems
MAKEFLAGS="-j14"
#-- Debugging flags
DEBUG_CFLAGS="-g"
DEBUG_CXXFLAGS="-g"
#DEBUG_RUSTFLAGS="-C debuginfo=2"

```

one note is the MAKEFLAGS="-j14" where 14 should be your total cpu thread count minus 2 (so you can do something while stuff compiles by having 1 core [each core has 2 thread's] available) since i have 8 cores 16 threads i use value 14 [16-2=14]


now in that same file well find the section that starts with:

```
# GLOBAL PACKAGE OPTIONS
#   These are default values for the options=() settings
```

and well change the content of it's `OPTIONS=` to the following

```
OPTIONS=(strip docs !libtool !staticlibs emptydirs zipman purge !debug lto)
```

doing so will enable Link Time Optimizations. 
this should make out AUR packages slightly faster

lastly we will change the section that start's with:

```
# COMPRESSION DEFAULTS
```
we will only change the first 4 options to read as follow:

```
COMPRESSGZ=(pigz -c -f -n)
COMPRESSBZ2=(pbzip2 -c -f)
COMPRESSXZ=(xz -c -z --threads=14 -)
COMPRESSZST=(zstd -c -z -q --threads=14 -)
```

finally save the document [CTRL+X] [y] [Return}

## Installing an AUR manager:
now that we have everything setup, we should install an AUR manager. 
here we will install yay.
as it is my preferred one. 

first enshure we have git installed `sudo pacman -S git`

```
mkdir temp
cd temp
git clone https://aur.archlinux.org/yay-bin.git
cd yay-bin
makepkg
makepkg --install
cd ..
cd ..
rm -rf ./temp
```

there you go. you can now install any aur packages by typing the following:
```
yay -S [package name] [second package] [etc]
```

to search for a package simply go to https://aur.archlinux.org/

## Setting-up Flatpak
Flatpak's are Distro agnostic linux packages that can esealy be installed and managed.
setting it up is straightforward.
```
sudo pacman -S flatpak
```
to find pakadges simply look at flathub:
* https://flathub.org

* Next ---->[AMD](..%2FGraphics%2Famd.md)
* Next(non amd users) ---->[Kernel options](..%2FKernel%2FKernel_options.md)
* [README](..%2FREADME.md)<---- Back to home