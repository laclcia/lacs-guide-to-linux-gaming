# Usefull Software
## Btop
One of the best process managers for linux is Btop. https://github.com/aristocratos/btop . 

installation is simple

* `sudo pacman -S btop`

## Input Remapper
Input Remapper allows you to remap any input to any other input in a per device basis. 

* https://github.com/sezanzeb/input-remapper/

to install simply run: 
```
yay -S input-remapper-git
```

## Krita
Want to edit images in a professional looking editor? don't want to use Gimp ? then try Krita!

* https://krita.org/en/

```
sudo pacman -S krita
```

## Flameshot
Want a good Screenshoting tool that will allow to not only take screenshot's but also crop and modify them?

* https://flameshot.org/
```
sudo pacman -S flameshot
```

## Kdenlive
Need a nonlinear video editor? Well do i have one for you! It's the one i personally use
* https://apps.kde.org/kdenlive/
```
sudo pacman -S kdenlive
```
## OBS
Need to record the screen, make videos or livestream? Open broadcaster is for you.
* https://obsproject.com/
Standard Install:
```
sudo pacman -S obs-studio
```
AMD GPU install:
```
yay -S obs-studio-amf
```
## Keepassxc
Need a password manager? well here is the best one available on ALL platforms. and no cloud so no possibility of data leaks.
* https://keepassxc.org/

```
sudo pacman -S keepassxc
```
## Inkscape
Need to edit some vectors? Inkscape is a great opensource vector image editor.
* https://inkscape.org/
```
sudo pacman -S inkscape
```

## Antimicrox
need to make a controller that is misbehaving?
antimictox can map any controller to be a generic xinput controller for steam and any other software that uses xinput.
* https://github.com/AntiMicroX/antimicroX
```
yay -S antimicrox
```
## Timeshift
this is a backup software. 
it will save you a LOT of headache.
install, launch then follow instructions to set it up.
it has full gui setup.
* https://github.com/linuxmint/timeshift
```
sudo pacman -S timeshift
```

* Next ---> [Wine](Wine.md)
* [README](..%2FREADME.md)<---- Back to home