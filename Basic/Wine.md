# Wine Is Not Emulator:
as the name suggests wine is a compatibility layer and not an emulator. WINE live translates windows call's to native linux ones. which is great for non-steam gaming.

First we should install wine and as we do so well also install a bunch of dependencies.

```
sudo pacman -S giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse libgpg-error lib32-libgpg-error alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo sqlite lib32-sqlite libxcomposite lib32-libxcomposite libxinerama lib32-libgcrypt libgcrypt lib32-libxinerama ncurses lib32-ncurses opencl-icd-loader lib32-opencl-icd-loader libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader lib32-gnutls lib32-libldap lib32-libgpg-error lib32-sqlite lib32-libpulse wine-staging wine-gecko wine-mono winetricks
```
now well update winetricks as the one on the repository can be a bit older than the current version. simply run:

```
sudo winetricks --self-update
```

now let's install wine packages for gaming.

note:

you may aldo need to install vcredit[year] for some games. i have not included them as they can cause problem's that require rebuilding the wine prefix.


simply run:
```
winetricks amstream avifil32 binkw32 cabinet cinepak cnc_ddraw d3dcompiler_42 d3dcompiler_43 d3dcompiler_46 d3dcompiler_47 d3drm d3dx10 d3dx10_43 d3dx11_42 d3dx11_43 d3dx9_24 d3dx9_25 d3dx9_26 d3dx9_27 d3dx9_28 d3dx9_29 d3dx9_30 d3dx9_31 d3dx9_32 d3dx9_33 d3dx9_34 d3dx9_35 d3dx9_36 d3dx9_37 d3dx9_38 d3dx9_39 d3dx9_40 d3dx9_41 d3dx9_42 d3dx9_43 d3dx9 d3dxof dinput8 dinput directmusic directplay dsound dswave dxvk vkd3d faudio winhttp wininet xact_x64 xinput comicsans corefonts droid cjkfonts 
```
this should install all you need to run most windows games/software easily.
* Next ---->[AUR setup](..%2FAUR%2FSetting_up.md)
* [README](..%2FREADME.md)<---- Back to home