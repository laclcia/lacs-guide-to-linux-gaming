# System Ricing

### What is ricing?

Ricing is the principle of modifying invisible files in linux 
(colloquially called dot files as in linux, starting a file name with a period makes it invisible).
And because those files have a period at the beginning, those dots are referred to as rice grains.
Thus ricing.
Usually that is mostly done for visual effect, but in our case, well focus exclusively on performances.

## Increase FD limit:

here we will increase the File Descriptor limit, also known as FD limit.
FD is an opaque handler that interfaces between user and kernel.
but we don't need to know the detail's here.
what you need to know is that we need more than default.
make sure you have nano installed `sudo pacman -S nano` to proceed further.

the symbol # in code blocks are comments

on Arch/SystemD:

```
sudo nano /etc/systemd/system.conf

#find the entry for #DefaultLimitNOFILE= and change it to the following

DefaultLimitNOFILE=1048576

#dont forget to remove the # character so the system uses the entry. 
#otherwise just like here it is a comment.
#once you have modified the file save and exit from nano [CTRL+X] [y] [Return}

sudo nano /etc/systemd/user.conf

#repeat the same steps as for the system.conf file
```

On Non systemD systems:

sudo nano /etc/security/limits.conf

## Fast pacman download:

it is possible to make pacman download Much faster by enabling parallel download. to do so simply do the following:

```
sudo nano /etc/pacman.conf
#then siply uncoment the line ParallelDownloads= and change the number to the amount of symultanious 
    download's you whant
```

## Setting up lib32 so we can run 32bit software:

simply follow instructions from the ARCHwiki:

https://wiki.archlinux.org/title/Official_repositories#Enabling_multilib

## The Arch User Repository:

----->[Setting_up](..%2FAUR%2FSetting_up.md)

## Chaotic AUR:

The chaotic aur is designed to provide Arch User Repository Packages that are precompiled. This may not be optimal compared to locally compiled but is a lifesaver when operating on a cpu limited computer.
Simply follow the instructions at:

https://aur.chaotic.cx/

## Positional Audio
enabling OpenAl positional audio for games is easy. simply run the following:

```
echo "hrtf = true" >> ~/.alsoftrc
```


## mouse pooling rate.
you can view the current mouse pooling rate with the following command
`systool -m usbhid -A mousepoll`

to change the value simply edit the file `/etc/modprobe.d/usbhid.conf` and change the value for `options usbhid mousepoll=` with the desired value in millisecondes.
esamples of valid values
* 0 = auto
* 1 = 1000 hz
* 2 = 500 hz
* 4 = 250 hz
* 8 = 125 hz

DO NOT USE VALUES THAT WOULD RESOLVE TO FLOATPOINT HZ VALUE



* Next ---->[Usefull_software](Usefull_software.md)
* [README](..%2FREADME.md)<---- Back to home
