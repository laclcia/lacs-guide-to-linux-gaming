# Zramswap
Zram is a method where we create a compressed ram-drive to fit more data on it than usual. 
Zramswap uses that method to make a swap partition in compressed ram to artificially increase fast ram. 
It's slightly slower than normal ram but MUCH faster than using a drive for swap. 
Now this is the easiest one to implement. 
Simply install `zramswap` package from the AUR and you are good to go. 
## Installation
```
yay -S zramswap
```
* Next ---->[HeroicGamesLauncher](..%2FGaming%2FLaunchers%2FHeroic.md)
* [README](..%2FREADME.md)<---- Back to home