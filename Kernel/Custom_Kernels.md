# Custom Kernels
## About
to improve performances we can compile and install a custom kernel.
custom kernels are similar to the default kernel
that comes with your distro while implementing changes that are beneficial for out use.

## Xanmod (recommended)
The Xanmod kernel is known to improve game performances in general.
It has loads of comparison benchmarks freely available from independent users on openbenchmarking.org.
* Benchmark example https://openbenchmarking.org/result/2001094-PTS-LIQUORIX33
### Installation
this is a bit more involved than usual.
first read this file and locate your cpu's number in the chart:
* https://aur.archlinux.org/cgit/aur.git/tree/choose-gcc-optimization.sh?h=linux-xanmod
* 
then run the following command:
```
yay -S --editmenu linux-xanmod linux-xanmod-headers
```
* when prompted about editing the build file type `A` then press `Enter`
if asked for the editor simply type `nano`.
* at line 15 (at the time of writing) where it says `_microarchitecture=0` replace 0 with your cpu arch number in the chart from earlier.

Example: `_microarchitecture=12` for a Zen 2 cpu

## Liquorix 
[TODO]

* Next ---->[zramswap](zramswap.md)
* [README](..%2FREADME.md)<---- Back to home