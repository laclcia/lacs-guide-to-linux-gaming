# Kernel Options & Settings

## Visible Grub menu and theaming

by default the grub menu will not appear when booting UNLESS you hold shift after the bios screen appears but before linux takes over. to change this well need to change the grub cinfigurations.

* step A) `sudo nano /etc/default/grub`
```
sudo nano /etc/default/grub
```
* step B) change the line `GRUB_TIMEOUT_STYLE="hidden"` to `GRUB_TIMEOUT_STYLE="menu"`
* step C) add the following 2 lines if unpresent if present edit them to match those 2  `GRUB_HIDDEN_TIMEOUT=10` & `GRUB_HIDDEN_TIMEOUT_QUIET=false`
* step D) uncomment the line `GRUB_GFXMODE="1920x1080"` replace `1920x1080` with your monitor native resolution.
* step E) save and exit. then run the command `sudo grub-mkconfig -o /boot/grub/grub.cfg` to apply the changes
```
sudo grub-mkconfig -o /boot/grub/grub.cfg
```
* Note  ) IN SOME CASES, YOUR THEME MAY NOT GO AS HIGH AS YOUR MONITOR IN SUCH CASES GO WITH THE HIGHEST RESOLUTION BOTH YOUR SCREEN AND THEME SUPPORTS.

Here you go. You should now see grub on every boot.

if you want to customize your grub theme, simply go to https://www.pling.com/browse?cat=109&ord=rating 

it is the biggest repository of grub themes out there. most of them will come with an install.py installation script that will automatically install it for you. otherwise manual installation instructions are on the right of the page if you scroll down a bit.

## kernel options
Kernel options are set in grub settings `/etc/default/grub`
```
sudo nano /etc/default/grub
```
kernel options go in the `GRUB_CMDLINE_LINUX_DEFAULT=` line as a space separated list. 

simply append to the list and dont remove what is already there

### transparent hugepage
transparent hugepage allows ram pages above 4K in size. the inner working's of witch are a bit too complex for the scope of this guide. simply this can increase performances in games and pieces of software that allocate and use a lot of ram

the argument to enable it is : `transparent_hugepage=always`

### Split lock detection
this is mainly for bug detection and will slow down software that trigger split lock in the kernel. this can safely be turned off 

the argument to disable it is : `split_lock_detect=off`

### no Audit logging
we can deactivate Audit's in kernel log. this marginally reduces line output to kernel logs slightly improving performance and reducing stuttering.

the arguments to disable it is : `audit=0 udev.log_priority=3`

### Amd Pstate driver
If you have an AMD Ryzen 3xxx or higher cpu you can enable the AMD pstate driver which enables the cpu to have much more than three power states. This can reduce power use and increase performances. 

the argument to enable it is : `amd_pstate=passive`

### AMD feature mask
if you have an AMD GPU set this to get full functionality including fan control, overclock and under-volt

the argument to enable it is : `amdgpu.ppfeaturemask=0xffffffff`
### example of full grub config
```
GRUB_CMDLINE_LINUX_DEFAULT="amd_pstate=passive transparent_hugepage=always clocksource=tsc audit=0 split_lock_detect=off apparmor=1 security=apparmor udev.log_priority=3 amdgpu.ppfeaturemask=0xffffffff libahci.ignore_sss=1"
```
#### Once done apply your setting's by running 
```
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

## gamermode kernel settings
There are kernel settings that can be tuned for gaming when needed.
The caveat is that those settings are suboptimal for a non-gaming task.
Switching between them can be a hassle to do manually.
I do have 2 scripts in my script repo called gamermode and normalmode that enables you to do the switch in one command each.
* https://gitlab.com/laclcia/laclcia-script-repo

simply follow instructions for installation in the repo
* Next ---->[Custom_Kernels.md](Custom_Kernels.md)
* [README](..%2FREADME.md)<---- Back to home